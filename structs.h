#ifndef STRUCTS_H
#define STRUCTS_H

struct Point
{
    double x = 0;
    double z = 0;
};

struct SystemParams
{
    double systemPeriod = 0; // период дискретизации расчетов
    double controlPeriod = 0; // период дискретизации уравления
    double chanelPeriod = 0; // период дискретизации обратных связей по координатам
    double K1 = 7.8;
    double K2 = 20;
};

struct Obstacles
{
    // характеристики траектории
    double R = 0; // радиус
    double Vx = 0; // течение вдоль оси X
    double Vz = 0; // течение вдоль оси Z
    double dX = 0; // постоянная ошибка датчика кооординаты X
    double dZ = 0; // постоянная ошибка датчика координаты Z
    double dVx = 0; // растущая со врмененем ошибка датчика кооординаты X
    double dVz = 0; // растущая со врмененем ошибка датчика координаты Z
};

struct SensorsParams
{
    // угол курса БИНС
    double psi0BINS = 0;
    double DwyBINS = 0;
    double psiBINSPeriod = 0.05;

    // угол курса магинтометра
    double psiAMagn = 0;
    double psiSMagn = 0;
    double psiMagnPeriod = 0.05;

    // северная скорость БИНС
    double Vx0BINS = 0;
    double axBINS = 0;
    double VxBINSPeriod = 0.05;

    // восточная скорость БИНС
    double Vz0BINS = 0;
    double azBINS = 0;
    double VzBINSPeriod = 0.05;

    // продольная скорость ГДЛ
    double mxLag = 0;
    double VxSLag = 0;
    double VxLagPeriod = 0.05;

    // поперечная скорость ГДЛ
    double mzLag = 0;
    double VzSLag = 0;
    double VzLagPeriod = 0.05;

    // X СНС
    double x0SNS = 0;
    double xSSNS = 0;
    double xSNSPeriod = 1;

    // Z СНС
    double z0SNS = 0;
    double zSSNS = 0;
    double zSNSPeriod = 1;

    // X ГАНС
    double xSGANS = 0;
    double xMGANS = 0;
    double xGANSPeriod = 60;

    // Z ГАНС
    double zSGANS = 0;
    double zMGANS = 0;
    double zGANSPeriod = 60;
};

struct FiltersParams
{
    double t1 = 0;
};

struct SensorsData
{
    // время
    double T;

    // угол курса БИНС
    double psiBINS = 0;

    // угол курса магинтометра
    double psiMagn = 0;

    // северная скорость БИНС
    double VxBINS = 0;

    // восточная скорость БИНС
    double VzBINS = 0;

    // продольная скорость ГДЛ
    double VxLag = 0;

    // поперечная скорость ГДЛ
    double VzLag = 0;

    // X СНС
    double xSNS = 0;

    // Z СНС
    double zSNS = 0;

    // X ГАНС
    double xGANS = 0;

    // Z ГАНС
    double zGANS = 0;
};

struct SSPData
{
    // время
    double T;

    // ССП по БИНС и ГДЛ
    double dxBINSLag = 0;
    double dzBINSLag = 0;
    double dVxBINSLag = 0;
    double dVzBINSLag = 0;

    // ССП по Магнитометру и ГДЛ
    double xMagnLag = 0;
    double zMagnLag = 0;

    // ССП по БИНС
    double xBINS = 0;
    double zBINS = 0;

    // ССП по ГАНС и БИНС
    double xBINSGANS = 0;
    double zBINSGANS = 0;

    // CCП по ГАНС и Магнитометру
    double xMagnGANS = 0;
    double zMagnGANS = 0;

    // ССП по СНС
    double dxSNS = 0;
    double dzSNS = 0;
};

struct FiltersData
{
    // время
    double T;

    // Калман по СНС и ССП по БИНС и ГДЛ
    double dxKalman1 = 0;
    double dzKalman1 = 0;
    double dvxKalman1 = 0;
    double dvzKalman1 = 0;

    // Комплексирование углов курса БИНС и Магнитометра
    double psiComplex1 = 0;
};

struct CalcStruct
{
    // расчет траектории

    // координаты заданной точки траектории
    double x_dist = 0;
    double z_dist = 0;

    // координаты текущей точки траектории
    double x_now = 0;
    double z_now = 0;

    // координаты в обратной связиъ
    double x_sens = 0;
    double z_sens = 0;

    // заданный угол курса
    double psi_dist[2] = {0,0};

    // текущий угол курса
    double psi_now = 0;

    // счетчик оборотов для абсолютного угла
    int count_psi = 0;

    // характеристики аппарата
    double m = 252; // масса
    double L11 = 10; // присоединенная масса по оси X
    double Iy = 258; // момент инерции вдоль оси Y
    double L55 = 260; // присоединенный момент инерции вдоль оси Y

    double Cx1 = 43.5; // гидродинамический коэффициент вдоль оси X
    double Cx2 = 4.35; // гидродинамический коэффициент вдоль оси X
    double Cwy1 = 140; // гидродинамический коэффициент вдоль оси Y
    double Cwy2 = 14; // гидродинамический коэффициент вдоль оси Y

    double Kdv = 5.3; // коэффициент усиления движителя
    double Tdv = 0.15; // постоянная времени движителя
    double Umax = 10; // максимальное напряжение движителя
    double a = 0.15; // расстояние между движителями

    // рассчитанные коэффициенты
    double Kdv_psi = 1.59; // коэффициент усиления ДРК контура курса
    double Kdv_x = 10.6; // коэффициент усиления ДРК контура марша

    // текущие переменные расчета
    double Upsi = 0; // задающее напряжение на ДРК контура курса
    double Ux = 0; // задающее напряжение на ДРК контура марша

    double wy = 0; // угловая скорость в контуре курса
    double Vx = 0; // маршевая скорость
    double dwy = 0; // угловое ускорение в контуре курса
    double dpsi_now = 0; // угловая скорость в контуре курса
    double dVx = 0; // ускорение к контуре марша
    double dpsi = 0; // производная угла курса для интегрирования

    double Fdx = 0; // упор ДРК в контуре марша
    double Mdy = 0; // упор ДРК в контуер курса
    double dFdx = 0; // производная упора ДРК в контуре марша
    double dMdy = 0; // производная упора ДРК в контуер курса

    double dx = 0; // производная координаты X
    double dz = 0; // производная координаты Z
};

struct KonturStruct
{
    // структура параметров контура
    double K1 = 7.8;
    double K2 = 20;

    // параметры расчета
    double systPeriod = 0.01; // период расчета системы
    double xPeriod = 0.1; // период обновления данных датчика координаты X
    double zPeriod = 0.1; // период обновления данных датчика координаты Z
};

struct TraekStruct
{
    // характеристики траектории
    double R = 0; // радиус
    double Vx = 0; // течение вдоль оси X
    double Vz = 0; // течение вдоль оси Z
    double dX = 0; // постоянная ошибка датчика кооординаты X
    double dZ = 0; // постоянная ошибка датчика координаты Z
    double dVx = 0; // растущая со врмененем ошибка датчика кооординаты X
    double dVz = 0; // растущая со врмененем ошибка датчика координаты Z

    // для расчетов
    double Vx_t = 0; // результат интегрирования растущей со временем ошибки датчика координаты X
    double Vz_t = 0; // результат интегрирования растущей со временем ошибки датчика координаты Z
};

struct SensorsStruct
{
    // структрура датчиков

    // угол курса БИНС
    double psiBINS = 0; // результирующий угол курса БИНС
    double psi0BINS = 0;
    double DwyBINS = 0;
    double DwyBINS_t = 0; // результат интегрирования
    double psiBINSPeriod = 0.05;

    // угол курса магинтометра
    double psiMagn = 0;
    double psiAMagn = 0;
    double psiSMagn = 0;
    double psiMagnPeriod = 0.05;

    // северная скорость БИНС
    double VxBINS = 0;
    double Vx0BINS = 0;
    double axBINS = 0;
    double VxBINSPeriod = 0.05;

    // восточная скорость БИНС
    double VzBINS = 0;
    double Vz0BINS = 0;
    double azBINS = 0;
    double VzBINSPeriod = 0.05;

    // продольная скорость ГДЛ
    double VxLag = 0;
    double mxLag = 0;
    double VxSLag = 0;
    double VxLagPeriod = 0.05;

    // поперечная скорость ГДЛ
    double VzLag = 0;
    double mzLag = 0;
    double VzSLag = 0;
    double VzLagPeriod = 0.05;

    // X СНС
    double xSNS = 0;
    double x0SNS = 0;
    double xSSNS = 0;
    double xSNSPeriod = 1;

    // Z СНС
    double zSNS = 0;
    double z0SNS = 0;
    double zSSNS = 0;
    double zSNSPeriod = 1;

    // X ГАНС
    double xGANS = 0;
    double xSGANS = 0;
    double xMGANS = 0;
    double xGANSPeriod = 60;

    // Z ГАНС
    double zGANS = 0;
    double zSGANS = 0;
    double zMGANS = 0;
    double zGANSPeriod = 60;
};

#endif // STRUCTS_H
