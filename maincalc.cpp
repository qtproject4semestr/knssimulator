#include "maincalc.h"

MainCalc::MainCalc(QObject *parent) : QObject(parent)
{
    // подключение
    udp = new UDPConnection(ReadConfig(),this);

    // обработка данных
    connect(udp,&UDPConnection::getAddPoint,this,&MainCalc::addAddPoint);
    connect(udp,&UDPConnection::getDeletePoint,this,&MainCalc::addDeletePoint);
    connect(udp,&UDPConnection::getContursParams,this,&MainCalc::addContursParams);
    connect(udp,&UDPConnection::getFiltersParams,this,&MainCalc::addFiltersParams);
    connect(udp,&UDPConnection::getObstcParams,this,&MainCalc::addObstcParams);
    connect(udp,&UDPConnection::getSensorsParams,this,&MainCalc::addSensorsParams);
    connect(udp,&UDPConnection::getStart,this,&MainCalc::addStart);

    // таймер
    systTimer = new QTimer(this);
    systTimer->setTimerType(Qt::PreciseTimer);

    qDebug()<<"KNSSimulator configured";
}

QJsonObject MainCalc::ReadConfig()
{
    QString name = "./config.json";
    QFile loadFile(name);
    if (!loadFile.open(QIODevice::ReadOnly|QIODevice::Text))
    {
        qDebug()<<"no file "<<name;
    }
    else
    {
        QByteArray rawData = loadFile.readAll();
        QJsonDocument m_confFile = QJsonDocument::fromJson(rawData);
        loadFile.close();
        if (!m_confFile.isObject())
            qDebug()<<"no config in file "<<name;
        else
        {
            QJsonObject obj = m_confFile.object();
            QJsonObject obj2 = obj["udpConnection"].toObject();
            return obj2;
        }
    }
}

void MainCalc::addAddPoint(Point data)
{
    pointsVec.append(data);
}

void MainCalc::addDeletePoint(int n)
{
    if (n<=pointsVec.size())
    {
        pointsVec.remove(n-1);
    }
}

void MainCalc::addContursParams(SystemParams data)
{
    kontrData.K1 = data.K1;
    kontrData.K2 =data.K2;
    kontrData.systPeriod = data.controlPeriod;
    kontrData.xPeriod = kontrData.zPeriod = data.chanelPeriod;
    Ttimer = data.systemPeriod;
}

void MainCalc::addObstcParams(Obstacles data)
{
    TraekStruct traek;
    traek.R = data.R;
    traek.Vx = data.Vx;
    traek.Vz = data.Vz;
    traek.dX = data.dX;
    traek.dZ = data.dZ;
    traek.dVx = data.dVx;
    traek.dVz = data.dVz;

    // записываем траекторию в список и добавляем в графики
    actualName = "dX="+QString::number(traek.dX,'f',1);
    traekList[actualName] = traek;
}

void MainCalc::addSensorsParams(SensorsParams data)
{
    sensorsData.psi0BINS = data.psi0BINS;
    sensorsData.DwyBINS = data.DwyBINS;
    sensorsData.psiBINSPeriod = data.psiBINSPeriod;

    sensorsData.psiAMagn = data.psiAMagn;
    sensorsData.psiSMagn = data.psiSMagn;
    sensorsData.psiMagnPeriod = data.psiMagnPeriod;

    sensorsData.Vx0BINS = data.Vx0BINS;
    sensorsData.axBINS = data.axBINS;
    sensorsData.VxBINSPeriod = data.VxBINSPeriod;

    sensorsData.Vz0BINS = data.Vz0BINS;
    sensorsData.azBINS = data.azBINS;
    sensorsData.VzBINSPeriod = data.VzBINSPeriod;

    sensorsData.mxLag = data.mxLag;
    sensorsData.VxSLag = data.VxSLag;
    sensorsData.VxLagPeriod = data.VxLagPeriod;

    sensorsData.mzLag = data.mzLag;
    sensorsData.VzSLag = data.VzSLag;
    sensorsData.VzLagPeriod = data.VzLagPeriod;

    sensorsData.x0SNS = data.x0SNS;
    sensorsData.xSSNS = data.xSSNS;
    sensorsData.xSNSPeriod = data.xSNSPeriod;

    sensorsData.z0SNS = data.z0SNS;
    sensorsData.zSSNS = data.zSSNS;
    sensorsData.zSNSPeriod = data.zSNSPeriod;

    sensorsData.xSGANS = data.xSGANS;
    sensorsData.xMGANS = data.xMGANS;
    sensorsData.xGANSPeriod = data.xGANSPeriod;

    sensorsData.zSGANS = data.zSGANS;
    sensorsData.zMGANS = data.zMGANS;
    sensorsData.zGANSPeriod = data.zGANSPeriod;
}

void MainCalc::addFiltersParams(FiltersParams data)
{

}

void MainCalc::addStart()
{
    calcData = new CalcStruct();
    calcData->x_dist = pointsVec.first().x;
    calcData->z_dist = pointsVec.first().z;
    actual_num = 1;

    // шаг расчета траектории
    T=0;

    // случайные величины
    std::normal_distribution<double> d_psiMagn_temp{0,sensorsData.psiSMagn}; // первое - среднее значение, второе - стандартное отклонение
    std::normal_distribution<double> d_xGANS_temp{0,sensorsData.xSGANS};
    std::normal_distribution<double> d_zGANS_temp{0,sensorsData.zSGANS};
    std::normal_distribution<double> d_xSNS_temp{0,sensorsData.xSSNS};
    std::normal_distribution<double> d_zSNS_temp{0,sensorsData.zSSNS};
    std::normal_distribution<double> d_vxLag_temp{0,sensorsData.VxSLag};
    std::normal_distribution<double> d_vzLag_temp{0,sensorsData.VzSLag};
    d_psiMagn = d_psiMagn_temp;
    d_xGANS = d_xGANS_temp;
    d_zGANS = d_zGANS_temp;
    d_xSNS = d_xSNS_temp;
    d_zSNS = d_zSNS_temp;
    d_vxLag = d_vxLag_temp;
    d_vzLag = d_vzLag_temp;

    // обнуление переменных для результатов интегрированя растущих со временем ошибок
    traekList[actualName].Vx_t = 0; // для получения растущей со временем ошибки координаты X
    traekList[actualName].Vz_t = 0; // для получения растущей со временем ошибки координаты Z
    sensorsData.DwyBINS_t = 0; // угол курса БИНС

    // определение счетчиков для периодов выдачи данных
    // СУ
    if (kontrData.systPeriod<Ttimer)
        kontrData.systPeriod = 1;
    else
    {
        //qDebug()<<" "<<kontrData.systPeriod/Ttimer;
        kontrData.systPeriod = kontrData.systPeriod/Ttimer;
    }
    // датчик координаты X
    if (kontrData.xPeriod<Ttimer)
        kontrData.xPeriod = 1;
    else
        kontrData.xPeriod = kontrData.xPeriod/Ttimer;
    // датчик координаты Y
    if (kontrData.zPeriod<Ttimer)
        kontrData.zPeriod = 1;
    else
        kontrData.zPeriod = kontrData.zPeriod/Ttimer;
    // отображение на графиках
    if (graphPeriod<Ttimer)
        graphPeriod = 1;
    else
        graphPeriod = graphPeriod/Ttimer;
    // угол курса БИНС
    if (sensorsData.psiBINSPeriod<Ttimer)
        sensorsData.psiBINSPeriod = 1;
    else
        sensorsData.psiBINSPeriod = sensorsData.psiBINSPeriod/Ttimer;
    // угол курса Магнитометра
    if (sensorsData.psiMagnPeriod<Ttimer)
        sensorsData.psiMagnPeriod = 1;
    else
        sensorsData.psiMagnPeriod = sensorsData.psiMagnPeriod/Ttimer;
    // северная скорость БИНС
    if (sensorsData.VxBINSPeriod<Ttimer)
        sensorsData.VxBINSPeriod = 1;
    else
        sensorsData.VxBINSPeriod = sensorsData.VxBINSPeriod/Ttimer;
    // восточная скорость БИНС
    if (sensorsData.VzBINSPeriod<Ttimer)
        sensorsData.VzBINSPeriod = 1;
    else
        sensorsData.VzBINSPeriod = sensorsData.VzBINSPeriod/Ttimer;
    // продольная скорость ГДЛ
    if (sensorsData.VxLagPeriod<Ttimer)
        sensorsData.VxLagPeriod = 1;
    else
        sensorsData.VxLagPeriod = sensorsData.VxLagPeriod/Ttimer;
    // поперечная скорость ГДЛ
    if (sensorsData.VzLagPeriod<Ttimer)
        sensorsData.VzLagPeriod = 1;
    else
        sensorsData.VzLagPeriod = sensorsData.VzLagPeriod/Ttimer;
    // координата X СНС
    if (sensorsData.xSNSPeriod<Ttimer)
        sensorsData.xSNSPeriod = 1;
    else
        sensorsData.xSNSPeriod = sensorsData.xSNSPeriod/Ttimer;
    // координата Z СНС
    if (sensorsData.zSNSPeriod<Ttimer)
        sensorsData.zSNSPeriod = 1;
    else
        sensorsData.zSNSPeriod = sensorsData.zSNSPeriod/Ttimer;
    // координата X ГАНС
    if (sensorsData.xGANSPeriod<Ttimer)
        sensorsData.xGANSPeriod = 1;
    else
        sensorsData.xGANSPeriod = sensorsData.xGANSPeriod/Ttimer;
    // координата Z ГАНС
    if (sensorsData.zGANSPeriod<Ttimer)
        sensorsData.zGANSPeriod = 1;
    else
        sensorsData.zGANSPeriod = sensorsData.zGANSPeriod/Ttimer;

    // счетчики для обновления данных
    systCount = 0; // СУ
    xCount = 0; // датчик координаты X
    zCount = 0; // датчик коордиаты Z
    graphCount = 0; // отображение графиков
    psiBINSCount = 0; // угол курса БИНС
    psiMagnCount = 0; // угол курса магнитометра
    VxBINSCount = 0; // северная скорсть БИНС
    VzBINSCount = 0; // восточная скорость БИНС
    VxLagCount = 0; // продольная скорость ГДЛ
    VzLagCount = 0; // поперечная скорость ГДЛ
    xSNSCount = 0; // координата X СНС
    zSNSCount = 0; // координата Z СНС
    xGANSCount = 0; // координата X ГАНС
    zGANSCount = 0; // координата Z ГАНС

    systTimer->start(100);
    connect(systTimer,&QTimer::timeout,this,&MainCalc::calcTraek);
}

void MainCalc::calcTraek()
{
    if (systCount == kontrData.systPeriod) // период дискретизации системы
    {
        Control_system();
        systCount = 0;
    }
    if (xCount == kontrData.xPeriod) // период датчика координаты X
    {
        calcData->x_sens = calcData->x_now+traekList[actualName].dX+traekList[actualName].Vx_t; //+rand() % 10;
        xCount = 0;
    }
    if (zCount == kontrData.zPeriod) // период датчиа координаты Z
    {
        calcData->z_sens = calcData->z_now+traekList[actualName].dZ+traekList[actualName].Vz_t; //+rand() % 10;
        zCount = 0;
    }
    if (psiBINSCount == sensorsData.psiBINSPeriod) // период угла курса БИНС
    {
        sensorsData.psiBINS = (calcData->psi_now + sensorsData.psi0BINS/Kc + sensorsData.DwyBINS_t/Kc)*Kc;
        psiBINSCount = 0;
    }
    if (psiMagnCount == sensorsData.psiMagnPeriod) // период угла курса магнитометра
    {
        sensorsData.psiMagn = (calcData->psi_now + sensorsData.psiAMagn*sin(calcData->psi_now)/Kc)*Kc;
        if (sensorsData.psiSMagn != 0)
            sensorsData.psiMagn += d_psiMagn(gen);
        psiMagnCount = 0;
    }
    if (VxBINSCount == sensorsData.VxBINSPeriod) // период северной скорости БИНС
    {
        sensorsData.VxBINS = calcData->dx+sensorsData.Vx0BINS+sensorsData.axBINS*cos(Wsh*T/Kc);
        VxBINSCount = 0;
    }
    if (VzBINSCount == sensorsData.VzBINSPeriod) // период восточной скорости БИНС
    {
        sensorsData.VzBINS = calcData->dz + sensorsData.Vz0BINS + sensorsData.azBINS*cos(Wsh*T/Kc);
        VzBINSCount = 0;
    }
    if (VxLagCount == sensorsData.VxLagPeriod) // период продольной скорости ГДЛ
    {
        sensorsData.VxLag = (1+sensorsData.mxLag)*(calcData->dx*cos(calcData->psi_now)-calcData->dz*sin(calcData->psi_now));
        if (sensorsData.VxSLag != 0)
            sensorsData.VxLag += d_vxLag(gen);
        VxLagCount = 0;
    }
    if (VzLagCount == sensorsData.VzLagPeriod) // период поперечной скорости ГДЛ
    {
        sensorsData.VzLag = (1+sensorsData.mzLag)*(calcData->dx*sin(calcData->psi_now)+calcData->dz*cos(calcData->psi_now));
        if (sensorsData.VzSLag != 0)
            sensorsData.VzLag += d_vzLag(gen);
        VzLagCount = 0;
    }
    if (xSNSCount == sensorsData.xSNSPeriod) // период координаты x СНС
    {
        sensorsData.xSNS = calcData->x_now+sensorsData.x0SNS;
        if (sensorsData.xSSNS != 0)
            sensorsData.xSNS+=d_xSNS(gen);
        xSNSCount = 0;
    }
    if (zSNSCount == sensorsData.zSNSPeriod) // период координаты z СНС
    {
        sensorsData.zSNS = calcData->z_now+sensorsData.z0SNS;
        if (sensorsData.zSSNS != 0)
            sensorsData.zSNS+=d_zSNS(gen);
        zSNSCount = 0;
    }
    if (xGANSCount == sensorsData.xGANSPeriod) // период координаты x ГАНС
    {
        sensorsData.xGANS = calcData->x_now;
        if (sensorsData.xSGANS != 0)
            sensorsData.xGANS+=d_xGANS(gen);
        xGANSCount = 0;
    }
    if (zGANSCount == sensorsData.zGANSPeriod) // период координаты z ГАНС
    {
        sensorsData.zGANS = calcData->z_now;
        if (sensorsData.zSGANS != 0)
            sensorsData.zGANS+=d_zGANS(gen);
        zGANSCount = 0;
    }
    if (pow(fabs(calcData->x_dist-calcData->x_sens),2)+pow(fabs(calcData->z_dist-calcData->z_sens),2) <= pow(traekList[actualName].R,2))
    {
        // дошли до очередной точки траектории
        if (actual_num<pointsVec.size())
        {
            calcData->x_dist = pointsVec[actual_num].x;
            calcData->z_dist = pointsVec[actual_num].z;
            actual_num++;
        }
        else
        {
            // дошли до последней точки траектории
            if (traekList[actualName].R != 1)
                traekList[actualName].R = 1;
            else
            {
                systTimer->stop();
                disconnect(systTimer,&QTimer::timeout,this,&MainCalc::calcTraek);
                udp->sendStop();
                delete calcData;
            }
        }
    }
    Runge(Ttimer); // интегрирование производим на каждом шаге
    T+=Ttimer; //увеличиваем время
    if (graphCount == graphPeriod) // период добавления точек в отображение траектории
    {
        SendGraphicData();
        graphCount = 0;
    }
    qDebug()<<"T "<<T<<" x "<<calcData->x_sens<<" z "<<calcData->z_sens<<" Vtz "<<traekList[actualName].Vz<<" psi "<<calcData->psi_now*Kc<<" Upsi "<<calcData->Upsi;
    if (T>MaxTime)
    {
        // превысили выделенное на моделированеи время
        systTimer->stop();
        disconnect(systTimer,&QTimer::timeout,this,&MainCalc::calcTraek);
        udp->sendStop();
        delete calcData;
    }

    // увеличиваем счетчики периодов
    systCount++;
    xCount++;
    zCount++;
    graphCount++;
    psiBINSCount++;
    psiMagnCount++;
    VxBINSCount++;
    VzBINSCount++;
    VxLagCount++;
    VzLagCount++;
    xSNSCount++;
    zSNSCount++;
    xGANSCount++;
    zGANSCount++;
}

void MainCalc::Control_system()
{
    // контура управления

    // контур марша - считаем задающее воздействие на двигатели
    calcData->Ux = calcData->Umax; // в моем случае разомкнутый максимум

    // контур курса - считаем задающее воздействие на двигатели
    double Psi_arctg = (atan2(calcData->z_dist-calcData->z_sens,calcData->x_dist-calcData->x_sens))*Kc; // угол курса как арктангенс разницы координат в диапазоне +-180
    // перевод в абсолютный угол
    /*if (Psi_arctg<0)
        Psi_arctg = 360+Psi_arctg; // переводим в диапазон 0-360*/
    if ((Psi_arctg-calcData->psi_dist[1]) > 170)  calcData->count_psi=calcData->count_psi-1;
    if ((Psi_arctg-calcData->psi_dist[1]) < (-170)) calcData->count_psi=calcData->count_psi+1;
    calcData->psi_dist[1]=Psi_arctg;
    calcData->psi_dist[0] = Psi_arctg+360*calcData->count_psi;
    // теперь формируем напряжение
    calcData->Upsi = (calcData->psi_dist[0]-calcData->psi_now*Kc)*kontrData.K1-calcData->wy*Kc*kontrData.K2;
    // учитываем ограничение напряжения
    if (calcData->Upsi>calcData->Umax)
        calcData->Upsi = calcData->Umax;
    else if (calcData->Upsi<-calcData->Umax)
        calcData->Upsi = -calcData->Umax;
}

void MainCalc::Vehicles_Model()
{
    // модель аппарата

    // контур марша
    calcData->dVx = (calcData->Fdx-calcData->Cx1*calcData->Vx*fabs(calcData->Vx)-calcData->Cx2*calcData->Vx)/(calcData->L11+calcData->m);

    // контур курса
    calcData->dwy = (calcData->Mdy-calcData->Cwy1*calcData->wy*fabs(calcData->wy)-calcData->Cwy2*calcData->wy)/(calcData->L55+calcData->Iy);
    calcData->dpsi = calcData->wy;

    // ДРК
    calcData->dFdx = (calcData->Kdv_x*calcData->Ux-calcData->Fdx)/calcData->Tdv;
    calcData->dMdy = (calcData->Kdv_psi*calcData->Upsi-calcData->Mdy)/calcData->Tdv;

    // контур координат
    calcData->dx = calcData->Vx*cos(calcData->psi_now)+traekList[actualName].Vx;
    calcData->dz = calcData->Vx*sin(calcData->psi_now)+traekList[actualName].Vz;
}

void MainCalc::Runge(double step)
{
    // создаем переменные для метода Рунге-Кутта 4-го порядка
    CalcStruct* tempCalc = new CalcStruct();
    double tempVx_t = 0;
    double tempVz_t = 0;
    double tempDwyBINS_t = 0;
    double tempdVx = 0;
    double tempdVz = 0;
    double tempDwyBINS = 0;


    // по лекциям
    Vehicles_Model();

    tempCalc->x_now = calcData->x_now;
    tempCalc->z_now = calcData->z_now;
    tempCalc->Vx = calcData->Vx;
    tempCalc->wy = calcData->wy;
    tempCalc->psi_now = calcData->psi_now;
    tempCalc->Fdx = calcData->Fdx;
    tempCalc->Mdy = calcData->Mdy;
    tempVx_t = traekList[actualName].Vx_t;
    tempVz_t = traekList[actualName].Vz_t;
    tempDwyBINS_t = sensorsData.DwyBINS_t;

    tempCalc->dx = calcData->dx;
    tempCalc->dz = calcData->dz;
    tempCalc->dVx = calcData->dVx;
    tempCalc->dwy = calcData->dwy;
    tempCalc->dpsi = calcData->dpsi;
    tempCalc->dFdx = calcData->dFdx;
    tempCalc->dMdy = calcData->dMdy;
    tempdVx = traekList[actualName].dVx;
    tempdVz = traekList[actualName].dVz;
    tempDwyBINS = sensorsData.DwyBINS;

    calcData->x_now = tempCalc->x_now+0.5*calcData->dx;
    calcData->z_now = tempCalc->z_now+0.5*calcData->dz;
    calcData->Vx = tempCalc->Vx+0.5*step*calcData->dVx;
    calcData->wy = tempCalc->wy+0.5*step*calcData->dwy;
    calcData->psi_now = tempCalc->psi_now+0.5*step*calcData->dpsi;
    calcData->Fdx = tempCalc->Fdx+0.5*step*calcData->dFdx;
    calcData->Mdy = tempCalc->Mdy+0.5*step*calcData->dMdy;
    traekList[actualName].Vx_t = tempVx_t+0.5*step*traekList[actualName].dVx;
    traekList[actualName].Vz_t = tempVx_t+0.5*step*traekList[actualName].dVz;
    sensorsData.DwyBINS_t = tempDwyBINS_t+0.5*step*sensorsData.DwyBINS;

    // второй подход
    Vehicles_Model();

    tempCalc->dx = tempCalc->dx + 2.0*calcData->dx;
    tempCalc->dz = tempCalc->dz + 2.0*calcData->dz;
    tempCalc->dVx = tempCalc->dVx + 2.0*calcData->dVx;
    tempCalc->dwy = tempCalc->dwy + 2.0*calcData->dwy;
    tempCalc->dpsi = tempCalc->dpsi + 2.0*calcData->dpsi;
    tempCalc->dFdx = tempCalc->dFdx + 2.0*calcData->dFdx;
    tempCalc->dMdy = tempCalc->dMdy + 2.0*calcData->dMdy;
    tempdVx = tempdVx + 2.0*traekList[actualName].dVx;
    tempdVz = tempdVz + 2.0*traekList[actualName].dVz;
    tempDwyBINS = tempDwyBINS + 2.0*sensorsData.DwyBINS;

    calcData->x_now = tempCalc->x_now+0.5*step*calcData->dx;
    calcData->z_now = tempCalc->z_now+0.5*step*calcData->dz;
    calcData->Vx = tempCalc->Vx+0.5*step*calcData->dVx;
    calcData->wy = tempCalc->wy+0.5*step*calcData->dwy;
    calcData->psi_now = tempCalc->psi_now+0.5*step*calcData->dpsi;
    calcData->Fdx = tempCalc->Fdx+0.5*step*calcData->dFdx;
    calcData->Mdy = tempCalc->Mdy+0.5*step*calcData->dMdy;
    traekList[actualName].Vx_t = tempVx_t+0.5*step*traekList[actualName].dVx;
    traekList[actualName].Vz_t = tempVx_t+0.5*step*traekList[actualName].dVz;
    sensorsData.DwyBINS_t = tempDwyBINS_t+0.5*step*sensorsData.DwyBINS;

    // третий подход
    Vehicles_Model();

    tempCalc->dx = tempCalc->dx + 2.0*calcData->dx;
    tempCalc->dz = tempCalc->dz + 2.0*calcData->dz;
    tempCalc->dVx = tempCalc->dVx + 2.0*calcData->dVx;
    tempCalc->dwy = tempCalc->dwy + 2.0*calcData->dwy;
    tempCalc->dpsi = tempCalc->dpsi + 2.0*calcData->dpsi;
    tempCalc->dFdx = tempCalc->dFdx + 2.0*calcData->dFdx;
    tempCalc->dMdy = tempCalc->dMdy + 2.0*calcData->dMdy;
    tempdVx = tempdVx + 2.0*traekList[actualName].dVx;
    tempdVz = tempdVz + 2.0*traekList[actualName].dVz;
    tempDwyBINS = tempDwyBINS + 2.0*sensorsData.DwyBINS;

    calcData->x_now = tempCalc->x_now+step*calcData->dx;
    calcData->z_now = tempCalc->z_now+step*calcData->dz;
    calcData->Vx = tempCalc->Vx+step*calcData->dVx;
    calcData->wy = tempCalc->wy+step*calcData->dwy;
    calcData->psi_now = tempCalc->psi_now+step*calcData->dpsi;
    calcData->Fdx = tempCalc->Fdx+step*calcData->dFdx;
    calcData->Mdy = tempCalc->Mdy+step*calcData->dMdy;
    traekList[actualName].Vx_t = tempVx_t+step*traekList[actualName].dVx;
    traekList[actualName].Vz_t = tempVx_t+step*traekList[actualName].dVz;
    sensorsData.DwyBINS_t = tempDwyBINS_t+step*sensorsData.DwyBINS;

    // четвертый подход
    Vehicles_Model();

    calcData->x_now = tempCalc->x_now+(step/6.0)*(tempCalc->dx + calcData->dx);
    calcData->z_now = tempCalc->z_now+(step/6.0)*(tempCalc->dz + calcData->dz);
    calcData->Vx = tempCalc->Vx+(step/6.0)*(tempCalc->dVx + calcData->dVx);
    calcData->wy = tempCalc->wy+(step/6.0)*(tempCalc->dwy + calcData->dwy);
    calcData->psi_now = tempCalc->psi_now+(step/6.0)*(tempCalc->dpsi + calcData->dpsi);
    calcData->Fdx = tempCalc->Fdx+(step/6.0)*(tempCalc->dFdx + calcData->dFdx);
    calcData->Mdy = tempCalc->Mdy+(step/6.0)*(tempCalc->dMdy + calcData->dMdy);
    traekList[actualName].Vx_t = tempVx_t+(step/6.0)*(tempdVx + traekList[actualName].dVx);
    traekList[actualName].Vz_t = tempVx_t+(step/6.0)*(tempdVz + traekList[actualName].dVz);
    sensorsData.DwyBINS_t = tempDwyBINS_t+(step/6.0)*(tempDwyBINS + sensorsData.DwyBINS);

    // методом прямоугольника - пока единственный рабоает
    /*Vehicles_Model();

    calcData->x_now += step*calcData->dx;
    calcData->z_now += step*calcData->dz;
    calcData->Vx += step*calcData->dVx;
    calcData->wy += step*calcData->dwy;
    calcData->psi_now += step*calcData->wy;
    calcData->Fdx += step*calcData->dFdx;
    calcData->Mdy += step*calcData->dMdy;
    traekList[actualName].Vx_t+= step*traekList[actualName].dVx;
    traekList[actualName].Vz_t += step*traekList[actualName].dVz;
    sensorsData.DwyBINS_t += step*sensorsData.DwyBINS;*/

    delete tempCalc;
}

void MainCalc::SendGraphicData()
{
    Point dataPoint;
    dataPoint.x = calcData->x_now;
    dataPoint.z = calcData->z_now;
    udp->sendTraek(dataPoint);

    SensorsData dataSensors;
    dataSensors.T = T;
    dataSensors.psiBINS = sensorsData.psiBINS;
    dataSensors.psiMagn = sensorsData.psiMagn;
    dataSensors.VxBINS = sensorsData.VxBINS;
    dataSensors.VzBINS = sensorsData.VzBINS;
    dataSensors.VxLag = sensorsData.VxLag;
    dataSensors.VzLag = sensorsData.VzLag;
    dataSensors.xSNS = sensorsData.xSNS;
    dataSensors.zSNS = sensorsData.zSNS;
    dataSensors.xGANS = sensorsData.xGANS;
    dataSensors.zGANS = sensorsData.zGANS;
    udp->sendSensors(dataSensors);

    FiltersData dataFilter;
    dataFilter.T = T;
    //udp->sendFilters(dataFilter);

    SSPData dataSSP;
    dataSSP.T = T;
    dataSSP.dxBINSLag = 0;
    dataSSP.dzBINSLag = 0;
    dataSSP.dVxBINSLag = 0;
    dataSSP.dVzBINSLag = 0;
    dataSSP.xMagnLag = 0;
    dataSSP.zMagnLag = 0;
    dataSSP.xBINS = 0;
    dataSSP.zBINS = 0;
    dataSSP.xBINSGANS = 0;
    dataSSP.zBINSGANS = 0;
    dataSSP.xMagnGANS = 0;
    dataSSP.zMagnGANS = 0;
    dataSSP.dxSNS = 0;
    dataSSP.dzSNS = 0;
    //udp->sendSSP(dataSSP);
}
