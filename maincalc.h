#ifndef MAINCALC_H
#define MAINCALC_H

#include <QObject>
#include <QTimer>
#include <math.h>
#include <random>

#include "Connection/udpconnection.h"
#include "structs.h"

#include "./shared_includes/kx_protocol.h"
#include "./shared_includes/qkx_coeffs.h"

extern double X[2000][2];
extern QVector<double> K;

#define Kc 57.3
#define Rz 6371000
#define g 9.81
#define Wsh 0.00124

class MainCalc : public QObject
{
    Q_OBJECT
public:
    explicit MainCalc(QObject *parent = nullptr);

private:

    // соединение
    UDPConnection* udp;

    QVector<Point> pointsVec;
    int curPoint = 0;
    bool isGoing = false;

    QTimer* systTimer; // таймер дискретизации системы

    CalcStruct* calcData; // расчеты траектории
    KonturStruct kontrData; // параметры контура
    SensorsStruct sensorsData; // данные датчиков

    QMap<QString,TraekStruct> traekList; // харакетристики рассчитанных траекторий
    QString actualName; // текущая траектория
    int actual_num = 0;
    double Ttimer = 0.1; // период расчетов
    double MaxTime = 500; // максимальная длительность моделирования

    // шаг расчета траектории
    double T=0; // время моделирования
    int graphPeriod = 1; // период вывода графиков

    // случайные величины
    std::random_device rd{};
    std::mt19937 gen{rd()};

    // случайные величины
    std::normal_distribution<double> d_psiMagn{0,sensorsData.psiSMagn}; // первое - среднее значение, второе - стандартное отклонение
    std::normal_distribution<double> d_xGANS{0,sensorsData.xSGANS};
    std::normal_distribution<double> d_zGANS{0,sensorsData.zSGANS};
    std::normal_distribution<double> d_xSNS{0,sensorsData.xSSNS};
    std::normal_distribution<double> d_zSNS{0,sensorsData.zSSNS};
    std::normal_distribution<double> d_vxLag{0,sensorsData.VxSLag};
    std::normal_distribution<double> d_vzLag{0,sensorsData.VzSLag};

    // счетчики для обновления данных
    int systCount = 0; // СУ
    int xCount = 0; // датчик координаты X
    int zCount = 0; // датчик коордиаты Z
    int graphCount = 0; // отображение графиков
    int psiBINSCount = 0; // угол курса БИНС
    int psiMagnCount = 0; // угол курса магнитометра
    int VxBINSCount = 0; // северная скорсть БИНС
    int VzBINSCount = 0; // восточная скорость БИНС
    int VxLagCount = 0; // продольная скорость ГДЛ
    int VzLagCount = 0; // поперечная скорость ГДЛ
    int xSNSCount = 0; // координата X СНС
    int zSNSCount = 0; // координата Z СНС
    int xGANSCount = 0; // координата X ГАНС
    int zGANSCount = 0; // координата Z ГАНС

public slots:

    // управление желаемой траекторией
    void addAddPoint(Point data); // добавить точку траектории
    void addDeletePoint(int n); // удалить точку траектории

    // управление контурами
    void addContursParams(SystemParams data);

    // управление воздействиями
    void addObstcParams(Obstacles data);

    // управление датчиками
    void addSensorsParams(SensorsParams data);

    // управление фильтрами
    void addFiltersParams(FiltersParams data);

    // управление моделированием
    void addStart();

private slots:

    // СУ
    void Control_system();

    // Модель
    void Vehicles_Model();

    // Интегрирование
    void Runge(double step);

    // Расчет траектории
    void calcTraek();

    // Отправка данных для графиков
    void SendGraphicData();

    // чтение фацла конфига
    QJsonObject ReadConfig();
};

#endif // MAINCALC_H
