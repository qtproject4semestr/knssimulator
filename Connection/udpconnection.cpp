#include "udpconnection.h"

UDPConnection::UDPConnection(QJsonObject config, QObject *parent) : QObject(parent)
{
    socket = new QUdpSocket(this);
    makeSocket(config);
}

void UDPConnection::makeSocket(QJsonObject obj)
{
    if (obj.keys().contains("receiver"))
    {
        QJsonObject obj1 = obj["receiver"].toObject();
        receiverAdress = QHostAddress(obj1["ip"].toString());
        receiverPort = obj1["port"].toInt();

        datagrammSend.setSender(receiverAdress,receiverPort);
        socket->bind(receiverAdress,receiverPort);

        connect(socket,&QAbstractSocket::readyRead,this,&UDPConnection::getData);
    }
    if (obj.keys().contains("sender"))
    {
        QJsonObject obj1 = obj["sender"].toObject();
        senderAdress = QHostAddress(obj1["ip"].toString());
        senderPort = obj1["port"].toInt();

        datagrammSend.setSender(receiverAdress,receiverPort);
        datagrammSend.setDestination(senderAdress,senderPort);
    }
}

void UDPConnection::getData()
{
    datagrammGet = socket->receiveDatagram();
    QByteArray data = datagrammGet.data();
    QList<QByteArray> list = data.split(',');
    QByteArray temp;
    qDebug()<<"got header "<<list[0];
    if (list[0] == "AddPoint")
    {
        temp = data.right(data.size()-9);
        Point dataGet;
        memcpy(&dataGet,temp,sizeof(Point));
        emit getAddPoint(dataGet);
    }
    else if (list[0] == "DeletePoint")
    {
        temp = data.right(data.size()-12);
        int dataGet = temp.toInt();
        emit getDeletePoint(dataGet);
    }
    else if (list[0] == "SetConturs")
    {
        temp = data.right(data.size()-11);
        SystemParams dataGet;
        memcpy(&dataGet,temp,sizeof(SystemParams));
        emit getContursParams(dataGet);
    }
    else if (list[0] == "SetSensors")
    {
        temp = data.right(data.size()-11);
        SensorsParams dataGet;
        memcpy(&dataGet,temp,sizeof(SensorsParams));
        emit getSensorsParams(dataGet);
    }
    else if (list[0] == "SetFilters")
    {
        temp = data.right(data.size()-11);
        FiltersParams dataGet;
        memcpy(&dataGet,temp,sizeof(FiltersParams));
        emit getFiltersParams(dataGet);
    }
    else if (list[0] == "SetObstacles")
    {
        temp = data.right(data.size()-13);
        Obstacles dataGet;
        memcpy(&dataGet,temp,sizeof(Obstacles));
        emit getObstcParams(dataGet);
    }
    else if (list[0] == "Start")
    {
        emit getStart();
    }
}

void UDPConnection::sendTraek(Point data)
{
    QByteArray array;
    array.resize(sizeof(Point));
    array = QByteArray::fromRawData((char*)&data,sizeof(Point));
    array.insert(0,"TraekData,");
    datagrammSend.setData(array);
    socket->writeDatagram(datagrammSend);
}

void UDPConnection::sendSensors(SensorsData data)
{
    QByteArray array;
    array.resize(sizeof(SensorsData));
    array = QByteArray::fromRawData((char*)&data,sizeof(SensorsData));
    array.insert(0,"SensorsData,");
    datagrammSend.setData(array);
    socket->writeDatagram(datagrammSend);
}

void UDPConnection::sendFilters(FiltersData data)
{
    QByteArray array;
    array.resize(sizeof(FiltersData));
    array = QByteArray::fromRawData((char*)&data,sizeof(FiltersData));
    array.insert(0,"FiltersData,");
    datagrammSend.setData(array);
    socket->writeDatagram(datagrammSend);
}

void UDPConnection::sendSSP(SSPData data)
{
    QByteArray array;
    array.resize(sizeof(SSPData));
    array = QByteArray::fromRawData((char*)&data,sizeof(SSPData));
    array.insert(0,"SSPData,");
    datagrammSend.setData(array);
    socket->writeDatagram(datagrammSend);
}

void UDPConnection::sendStop()
{
    QByteArray array = "Stop";
    datagrammSend.setData(array);
    socket->writeDatagram(datagrammSend);
}
