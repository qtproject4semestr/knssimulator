#ifndef UDPCONNECTION_H
#define UDPCONNECTION_H

#include <QObject>
#include <QUdpSocket>
#include <QNetworkDatagram>
#include <QHostAddress>

#include <QJsonDocument>
#include <QJsonObject>
#include <QFile>
#include <QDir>

#include "structs.h"

class UDPConnection : public QObject
{
    Q_OBJECT
public:
    explicit UDPConnection(QJsonObject config, QObject *parent = nullptr);

private:
    QUdpSocket* socket;
    QNetworkDatagram datagrammGet;
    QNetworkDatagram datagrammSend;
    QHostAddress receiverAdress;
    QHostAddress senderAdress;
    int receiverPort;
    int senderPort;

signals:

    // управление желаемой траекторией
    void getAddPoint(Point data); // добавить точку траектории
    void getDeletePoint(int n); // удалить точку траектории

    // управление контурами
    void getContursParams(SystemParams data);

    // управление воздействиями
    void getObstcParams(Obstacles data);

    // управление датчиками
    void getSensorsParams(SensorsParams data);

    // управление фильтрами
    void getFiltersParams(FiltersParams data);

    // управление моделированием
    void getStart();

private slots:
   void makeSocket(QJsonObject obj);
   void getData();

public slots:

   // получили данные траектори
   void sendTraek(Point data);

   // получили данные датчиков
   void sendSensors(SensorsData data);

   // получили данные фильтров
   void sendFilters(FiltersData data);

   // получили данные ССП
   void sendSSP(SSPData data);

   // расчет завершен
   void sendStop();

};

#endif // UDPCONNECTION_H
